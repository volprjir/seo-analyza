# Create your views here.
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.encoding import force_text, force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode

from user_dashboard.forms import SignUpForm
from user_dashboard.tokens import account_activation_token


@login_required(login_url='/dash/login/')
def index(request):
    return render(request, 'user_dashboard/index.html')


def user_login(request):
    if request.method == 'GET':
        template = 'user_dashboard/login.html'
        context = {}
        next = request.GET.get('next', None)
        if next:
            context['next'] = next
        return render(request, template, context)
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/dash/')
            else:
                return render(request, 'user_dashboard/login.html', {'error': 'User is not active'})
        else:
            return render(request, 'user_dashboard/login.html', {'error': 'Wrong credentials'})
    else:
        return render(request, 'user_dashboard/login.html', {'error': 'User is not active'})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your analyzaseo.cz account.'
            message = render_to_string('user_dashboard/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            return HttpResponse('Please confirm your email address to complete the website')
    else:
        form = SignUpForm()
    return render(request, 'user_dashboard/signup.html', {'form': form})


def account_activation_sent(request):
    return render(request, 'user_dashboard/account_activation_sent.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
        return render(request, 'user_dashboard/activated_user.html')
    else:
        return HttpResponse('Activation link is invalid!')
